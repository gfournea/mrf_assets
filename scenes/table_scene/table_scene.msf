<scene>
  <lights>  
    <area_light>
      <emittance name="Area" type="diffuse">
        <radiance value="60.0" />
        <spectrum value="0:1.0,1000:1.0" />
      </emittance>
      <transformations>
        <transform>
          <scale x="0.42579565024895594" y="0.42579565024895594" z="1.0" />
          <translation x="1.4825880527496338" y="1.1088827848434448" z="-0.9053518772125244" />
          <rotation>
            <angle value="113.11383393885404" />
            <axis x="-0.7447815537452698" y="0.6555641889572144" z="-0.12464448064565659" />
          </rotation>
        </transform>
      </transformations>
    </area_light>
  </lights>
  
  <materials>

    <!----------------------------------------------------------------------------------->
    <!-- METAL PART -->
    <!----------------------------------------------------------------------------------->
    

    <material alpha_g="0.05" name="SharpIron" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>    
    <material alpha_g="0.32" name="RoughIron" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material alpha_g="0.15" name="Copper" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Cu/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Cu/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.01" name="SharpAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material alpha_g="0.2" name="RoughAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.05" name="SharpGold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material alpha_g="0.15" name="RoughGold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <!----------------------------------------------------------------------------------->
    <!-- SWORD PART -->
    <!----------------------------------------------------------------------------------->
    
    <material name="Handle" type="PhongNormalized">
      <diffuse>
        <albedo value="0.85"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
      </diffuse>
      <specular exponent="5">
        <albedo value="0.15"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
      </specular>
    </material>
    
    <!----------------------------------------------------------------------------------->
    <!-- MACBETH PART -->
    <!----------------------------------------------------------------------------------->
    
    <material name="1_darkskin" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/1_darkskin.spd" />
    </material>    
    <material name="2_lightskin" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/2_lightskin.spd" />
    </material>
    <material name="3_bluesky" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/3_bluesky.spd" />
    </material>    
    <material name="4_foliage" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/4_foliage.spd" />
    </material>
    <material name="5_blueflower" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/5_blueflower.spd" />
    </material>    
    <material name="6_bluishgreen" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/6_bluishgreen.spd" />
    </material>
    <material name="7_orange" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/7_orange.spd" />
    </material>    
    <material name="8_purplishblue" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/8_purplishblue.spd" />
    </material>
    <material name="9_lightred" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/9_lightred.spd" />
    </material>    
    <material name="10_purple" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/10_purple.spd" />
    </material>
    <material name="11_yellowgreen" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/11_yellowgreen.spd" />
    </material>    
    <material name="12_orangeyellow" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/12_orangeyellow.spd" />
    </material>
    <material name="13_blue" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/13_blue.spd" />
    </material>    
    <material name="14_green" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/14_green.spd" />
    </material>
    <material name="15_red" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/15_red.spd" />
    </material>    
    <material name="16_yellow" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/16_yellow.spd" />
    </material>
    <material name="17_magenta" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/17_magenta.spd" />
    </material>    
    <material name="18_cyan" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/18_cyan.spd" />
    </material>
    <material name="19_white" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
    </material>    
    <material name="20_neutral8" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/20_neutral8.spd" />
    </material>
    <material name="21_neutral65" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/21_neutral65.spd" />
    </material>    
    <material name="22_neutral5" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/22_neutral5.spd" />
    </material>
    <material name="23_neutral35" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
    </material>    
    <material name="24_black" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/24_black.spd" />
    </material>
    
    <!----------------------------------------------------------------------------------->
    <!-- MISC PART -->
    <!----------------------------------------------------------------------------------->
    
    <material name="Rock" type="PhongNormalized">
      <diffuse>
        <albedo value="0.85"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.15"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
      </specular>
    </material>

    <material name="Ground" type="PhongNormalized">
      <diffuse>
        <albedo value="0.9"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/11_yellowgreen.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.1"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="Dragon base" type="principled">
      <base_color>
        <spectrum file="../../mrf_assets/macbeth_spectrum/24_black.spd" />
      </base_color>
      <roughness value="0.19166666269302368" />
      <subsurface value="0.0" />
      <sheen value="0.0" />
      <sheenTint value="0.5" />
      <metallic value="0.9416666626930237" />
      <specular value="0.4000000059604645" />
      <specularTint value="0.0" />
      <anisotropic value="0.0" />
      <clearcoat value="0.0" />
      <clearcoatGloss value="0.029999999329447746" />
    </material>
    
    <material name="Wood" type="PhongNormalizedTextured" texture_repeat="4.0">
      <diffuse>
        <albedo value="0.65"/>        
      <!-- Problem with RGB2SPEC, need to scale the texture by its band number and force rgb = 1-->
        <spectrum value="0:32.0,1000:32.0" />
        <rgb_color b="1.0" g="1.0" r="1.0" />
      </diffuse>
      <diffuse_texture file="./textures/kitchen_wood_diff_1k.png"/>
      <diffuse_texture_spectral file="./textures/kitchen_wood_diff_1k_SPEC32.hdr"/>
      <!-- <normal_map file="./textures/kitchen_wood_nor_1k.png"/> -->
      <exponent_map file="./textures/kitchen_wood_rough_1k.png"/>
      <specular exponent="500">
        <albedo value="0.35"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>

    <material name="Probe inside" type="Lambert">
      <albedo value="1.0" />
      <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
    </material>

    <material name="Cloth" type="Lambert">
      <albedo value="1.0" />
      <spectrum file="../../mrf_assets/macbeth_spectrum/15_red.spd" />
    </material>

    <material name="Sand" type="Lambert">
      <albedo value="1.0" />
      <spectrum file="./table_scene_assets/Sand.spd" />
    </material>

    <material name="floor_shadow" type="ShadowCatcher"/>

    <!----------------------------------------------------------------------------------->
    <!-- DRAGON PART -->
    <!----------------------------------------------------------------------------------->
    
    <!-- <material name="Body" type="PhongNormalized">
      <diffuse>
        <albedo value="0.65"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/blue.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.35"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/green.spd" />
      </specular>
    </material> -->

    <!-- <material name="Body" type="principled"> -->
      <!-- <base_color> -->
        <!-- <spectrum file="../../mrf_assets/macbeth_spectrum/light_green.spd" /> -->
      <!-- </base_color> -->
      <!-- <roughness value="0.3583333492279053" /> -->
      <!-- <subsurface value="0.0" /> -->
      <!-- <sheen value="0.0" /> -->
      <!-- <sheenTint value="0.5" /> -->
      <!-- <metallic value="0.3333333134651184" /> -->
      <!-- <specular value="0.9416666626930237" /> -->
      <!-- <specularTint value="0.0" /> -->
      <!-- <anisotropic value="0.0" /> -->
      <!-- <clearcoat value="0.0" /> -->
      <!-- <clearcoatGloss value="0.029999999329447746" /> -->
    <!-- </material> -->

    <material alpha_g="0.25" name="Body" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="./table_scene_assets/spd/body_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="./table_scene_assets/spd/body_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material name="Bones" type="principled">
     <base_color>
       <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
     </base_color>
     <roughness value="0.5" />
     <subsurface value="0.5" />
     <sheen value="0.0" />
     <sheenTint value="0." />
     <metallic value="0.0" />
     <specular value="0.9416666626930237" />
     <specularTint value="0.0" />
     <anisotropic value="0.0" />
     <clearcoat value="0.1" />
     <clearcoatGloss value="0.3" />
    </material>

    <material name="Eye" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.5"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/10_purple.spd" />
      </specular>
    </material>

    <!-- <material name="Scales" type="principled"> -->
      <!-- <base_color> -->
        <!-- <spectrum file="../../mrf_assets/macbeth_spectrum/brown.spd" /> -->
      <!-- </base_color> -->
      <!-- <roughness value="0.19166666269302368" /> -->
      <!-- <subsurface value="0.0" /> -->
      <!-- <sheen value="0.0" /> -->
      <!-- <sheenTint value="0.5" /> -->
      <!-- <metallic value="0.9416666626930237" /> -->
      <!-- <specular value="0.4000000059604645" /> -->
      <!-- <specularTint value="0.0" /> -->
      <!-- <anisotropic value="0.0" /> -->
      <!-- <clearcoat value="0.0" /> -->
      <!-- <clearcoatGloss value="0.029999999329447746" /> -->
    <!-- </material> -->
    
    <material alpha_g="0.15" name="Scales" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="./table_scene_assets/spd/scales_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="./table_scene_assets/spd/scales_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material name="Teeth" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.5"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
  
    <!----------------------------------------------------------------------------------->
    <!-- GLASS PART -->
    <!----------------------------------------------------------------------------------->
    

    <!-- <material alpha_g="0.2" name="WhiteGlass" type="walter_bsdf"> -->
    <!-- <!-- <material name="BlueGlass" type="FresnelGlass"> --> -->
      <!-- <extinction> -->
        <!-- <spectrum file="../../mrf_assets/macbeth_spectrum/white.spd" /> -->
      <!-- </extinction> -->
      <!-- <fresnel> -->
        <!-- <eta> -->
          <!-- <spectrum value="0:1.45,1000:1.45" /> -->
        <!-- </eta> -->
        <!-- <kappa> -->
          <!-- <spectrum value="0:0,1000:0" /> -->
        <!-- </kappa> -->
      <!-- </fresnel> -->
    <!-- </material> -->

    <material alpha_g="0.2" name="BlueGlass" type="walter_bsdf">
    <!-- <material name="BlueGlass" type="FresnelGlass"> -->
      <extinction>
        <spectrum file="./table_scene_assets/spd/blue.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.2" name="RedGlass" type="walter_bsdf">
    <!-- <material name="RedGlass" type="FresnelGlass"> -->
      <extinction>
        <spectrum file="./table_scene_assets/spd/red.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.2" name="GreenGlass" type="walter_bsdf">
    <!-- <material name="GreenGlass" type="FresnelGlass"> -->
      <extinction>
        <spectrum file="./table_scene_assets/spd/green.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>    
    
    <material alpha_g="0.05" name="CustomGlass" type="walter_bsdf">
      <fresnel>
        <eta>
          <spectrum file="./table_scene_assets/spd/dielec_custom_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="./table_scene_assets/spd/dielec_custom_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
  </materials>
  <shapes>
    
    <!----------------------------------------------------------------------------------->
    <!-- PROBE PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="Probe inside">
      <mesh>
        <obj file="./table_scene_assets/globe_in.obj" />
      </mesh>
    </shape>
    <shape ref_material="GreenGlass">
      <mesh>
        <obj file="./table_scene_assets/globe_out.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpAlu">
      <mesh>
        <obj file="./table_scene_assets/base_in.obj" />
      </mesh>
    </shape>
    <shape ref_material="RedGlass">
      <mesh>
        <obj file="./table_scene_assets/base_out.obj" />
      </mesh>
    </shape>
    
    <!----------------------------------------------------------------------------------->
    <!-- LUCY PART -->
    <!----------------------------------------------------------------------------------->
    
    
    <shape ref_material="CustomGlass">
      <mesh>
        <obj file="./table_scene_assets/Lucy.obj" />
      </mesh>
    </shape>

    
    <!----------------------------------------------------------------------------------->
    <!-- GROUND PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="Rock">
      <mesh>
        <obj file="./table_scene_assets/SmallRock.obj" />
      </mesh>
    </shape>    
    <shape ref_material="Rock">
      <mesh>
        <obj file="./table_scene_assets/Rock.obj" />
      </mesh>
    </shape>
    <shape ref_material="Sand">
      <mesh>
        <obj file="./table_scene_assets/Sand.obj" />
      </mesh>
    </shape>
    <shape ref_material="Dragon base">
      <mesh>
        <obj file="./table_scene_assets/GroundSupport.obj" />
      </mesh>
    </shape>
    
    <!----------------------------------------------------------------------------------->
    <!-- DRAGON PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="Scales">
      <mesh>
        <obj file="./table_scene_assets/Scales.obj" />
      </mesh>
    </shape>    
    <shape ref_material="Bones">
      <mesh>
        <obj file="./table_scene_assets/Teeth.obj" />
      </mesh>
    </shape>
    <shape ref_material="Bones">
      <mesh>
        <obj file="./table_scene_assets/RearFaceHorns.obj" />
      </mesh>
    </shape>
    <shape ref_material="Bones">
      <mesh>
        <obj file="./table_scene_assets/Horns.obj" />
      </mesh>
    </shape>
    <shape ref_material="Scales">
      <mesh>
        <obj file="./table_scene_assets/Jaws.obj" />
      </mesh>
    </shape>
    <shape ref_material="Eye">
      <mesh>
        <obj file="./table_scene_assets/Eyes.obj" />
      </mesh>
    </shape>
    <shape ref_material="Bones">
      <mesh>
        <obj file="./table_scene_assets/Fangs.obj" />
      </mesh>
    </shape>
    <shape ref_material="Bones">
      <mesh>
        <obj file="./table_scene_assets/FootFangs.obj" />
      </mesh>
    </shape>    
    <!-- <shape ref_material="Bones"> -->
      <!-- <mesh> -->
        <!-- <obj file="./table_scene_assets/TailHorns.obj" /> -->
      <!-- </mesh> -->
    <!-- </shape> -->
    <shape ref_material="Body">
      <mesh>
        <obj file="./table_scene_assets/Body.obj" />
      </mesh>
    </shape>
    
    <!----------------------------------------------------------------------------------->
    <!-- SWORD PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="Copper">
      <mesh>
        <obj file="./table_scene_assets/SwordDecoration.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughIron">
      <mesh>
        <obj file="./table_scene_assets/SwordDecoration2.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughGold">
      <mesh>
        <obj file="./table_scene_assets/SwordDecoration3.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughIron">
      <mesh>
        <obj file="./table_scene_assets/SwordGuard.obj" />
      </mesh>
    </shape>
    <shape ref_material="Cloth">
      <mesh>
        <obj file="./table_scene_assets/SwordHandle.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughIron">
      <mesh>
        <obj file="./table_scene_assets/Blade.obj" />
      </mesh>
    </shape>
    <shape ref_material="Cloth">
      <mesh>
        <obj file="./table_scene_assets/Cloth.obj" />
      </mesh>
    </shape>
    
    <!----------------------------------------------------------------------------------->
    <!-- TABLE PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="Wood">
      <mesh>
        <obj file="./table_scene_assets/Top.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wood">
      <mesh>
        <obj file="./table_scene_assets/Base.obj" />
      </mesh>
    </shape>
    
        
    <!----------------------------------------------------------------------------------->
    <!-- MACBETH PART -->
    <!----------------------------------------------------------------------------------->
    
    <shape ref_material="10_purple">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_10_purple.obj" />
      </mesh>
    </shape>
    <shape ref_material="11_yellowgreen">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_11_yellowgreen.obj" />
      </mesh>
    </shape>
    <shape ref_material="12_orangeyellow">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_12_orangeyellow.obj" />
      </mesh>
    </shape>
    <shape ref_material="13_blue">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_13_blue.obj" />
      </mesh>
    </shape>
    <shape ref_material="14_green">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_14_green.obj" />
      </mesh>
    </shape>
    <shape ref_material="15_red">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_15_red.obj" />
      </mesh>
    </shape>
    <shape ref_material="16_yellow">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_16_yellow.obj" />
      </mesh>
    </shape>
    <shape ref_material="17_magenta">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_17_magenta.obj" />
      </mesh>
    </shape>
    <shape ref_material="18_cyan">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_18_cyan.obj" />
      </mesh>
    </shape>
    <shape ref_material="19_white">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_19_white.obj" />
      </mesh>
    </shape>
    <shape ref_material="1_darkskin">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_1_darkskin.obj" />
      </mesh>
    </shape>
    <shape ref_material="20_neutral8">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_20_neutral8.obj" />
      </mesh>
    </shape>
    <shape ref_material="21_neutral65">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_21_neutral65.obj" />
      </mesh>
    </shape>
    <shape ref_material="22_neutral5">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_22_neutral5.obj" />
      </mesh>
    </shape>
    <shape ref_material="23_neutral35">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_23_neutral35.obj" />
      </mesh>
    </shape>
    <shape ref_material="24_black">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_24_black.obj" />
      </mesh>
    </shape>
    <shape ref_material="2_lightskin">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_2_lightskin.obj" />
      </mesh>
    </shape>
    <shape ref_material="3_bluesky">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_3_bluesky.obj" />
      </mesh>
    </shape>
    <shape ref_material="4_foliage">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_4_foliage.obj" />
      </mesh>
    </shape>
    <shape ref_material="5_blueflower">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_5_blueflower.obj" />
      </mesh>
    </shape>
    <shape ref_material="6_bluishgreen">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_6_bluishgreen.obj" />
      </mesh>
    </shape>
    <shape ref_material="7_orange">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_7_orange.obj" />
      </mesh>
    </shape>
    <shape ref_material="8_purplishblue">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_8_purplered.obj" />
      </mesh>
    </shape>
    <shape ref_material="9_lightred">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_9_moderatered.obj" />
      </mesh>
    </shape>
    <shape ref_material="24_black">
      <mesh>
        <obj file="./table_scene_assets/Macbeth_support.obj" />
      </mesh>
    </shape>
    
  </shapes>

  <ext_resources>  
  <resources_path path="./textures"/>
    <envmaps>
      <panoramic power_scaling="1.0" file="envmap_blurr.exr" name="my_envmap" phi="-120" theta="0" />
      
      <!-- <!-- Problem with RGB2SPEC, need to scale the texture by its band number --> -->
      <panoramic power_scaling="1.0" file="envmap_blurr_SPEC32.hdr" name="my_envmap_spectral" phi="-120" theta="0" />
            <!-- <panoramic power_scaling="1.0" file="skydome.exr" name="my_envmap" /> -->
      
      <!-- Problem with RGB2SPEC, need to scale the texture by its band number -->
      <!-- <panoramic power_scaling="1.0" file="skydome.exr" name="my_envmap_spectral"/> -->
      </envmaps>
  </ext_resources>

  <background type="envmap" ref_envmap="my_envmap" ref_envmap_spectral="my_envmap_spectral"></background>
  
</scene>
