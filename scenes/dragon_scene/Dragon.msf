<scene>
  <lights>
	</lights>
  <materials>

    <material alpha_g="0.05" name="SharpMetal" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material name="Body" type="PhongNormalized">
      <diffuse>
        <albedo value="0.65"/>
        <rgb_color r="0.29" g="0.60" b="0.29" />
        <spectrum file="../../macbeth_spectrum/green.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.35"/>
        <rgb_color r="0.17" g="0.25" b="0.59" />
        <spectrum file="../../macbeth_spectrum/blue.spd" />
      </specular>
    </material>

    <material name="Handle" type="PhongNormalized">
      <diffuse>
        <albedo value="0.85"/>
        <rgb_color r="0.33" g="0.33" b="0.33" />
        <spectrum file="../../macbeth_spectrum/gray_008.spd" />
      </diffuse>
      <specular exponent="5">
        <albedo value="0.15"/>
        <rgb_color r="0.33" g="0.33" b="0.33" />
        <spectrum file="../../macbeth_spectrum/gray_008.spd" />
      </specular>
    </material>

    <material name="Eye" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <rgb_color r="1.0" g="1.0" b="1.0" />
        <spectrum file="../../macbeth_spectrum/white.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.5"/>
        <rgb_color r="0.37" g="0.24" b="0.42" />
        <spectrum file="../../macbeth_spectrum/purple.spd" />
      </specular>
    </material>

    <material name="Rock" type="PhongNormalized">
      <diffuse>
        <albedo value="0.85"/>
        <rgb_color r="0.33" g="0.33" b="0.33" />
        <spectrum file="../../macbeth_spectrum/gray_008.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.15"/>
        <rgb_color r="0.33" g="0.33" b="0.33" />
        <spectrum file="../../macbeth_spectrum/gray_008.spd" />
      </specular>
    </material>

    <material name="Ground" type="PhongNormalized">
      <diffuse>
        <albedo value="0.9"/>
        <rgb_color r="0.96" g="0.81" b="0.09" />
        <spectrum file="../../macbeth_spectrum/yellow.spd" />
      </diffuse>
      <specular exponent="5">
        <albedo value="0.1"/>
        <rgb_color r="1.0" g="1.0" b="1.0" />
        <spectrum file="../../macbeth_spectrum/white.spd" />
      </specular>
    </material>

    <material name="Scales" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <rgb_color r="0.47" g="0.33" b="0.27" />
        <spectrum file="../../macbeth_spectrum/brown.spd" />
      </diffuse>
      <specular exponent="10">
        <albedo value="0.5"/>
        <rgb_color r="0.96" g="0.81" b="0.09" />
        <spectrum file="../../macbeth_spectrum/yellow.spd" />
      </specular>
    </material>

    <material name="Support" type="Lambert">
      <albedo value="1.0" />
      <rgb_color r="0.79" g="0.6" b="0.52" />
      <spectrum file="../../macbeth_spectrum/light_brown.spd" />
    </material>

    <material alpha_g="0.15" name="Copper" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Cu/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Cu/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.1" name="Gold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.32" name="RoughMetal" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material name="Teeth" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <rgb_color r="0.65" g="0.65" b="0.65" />
        <spectrum file="../../macbeth_spectrum/gray_035.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.5"/>
        <rgb_color r="1.0" g="1.0" b="1.0" />
        <spectrum file="../../macbeth_spectrum/white.spd" />
      </specular>
    </material>

  </materials>
  <shapes>
    <shape ref_material="SharpMetal">
      <mesh>
        <obj file="./Dragon_assets/Blade.obj" />
      </mesh>
    </shape>
    <shape ref_material="Body">
      <mesh>
        <obj file="./Dragon_assets/Body.obj" />
      </mesh>
    </shape>
    <shape ref_material="Handle">
      <mesh>
        <obj file="./Dragon_assets/Cloth.obj" />
      </mesh>
    </shape>
    <shape ref_material="Eye">
      <mesh>
        <obj file="./Dragon_assets/Eyes.obj" />
      </mesh>
    </shape>
    <shape ref_material="Rock">
      <mesh>
        <obj file="./Dragon_assets/Rock.obj" />
      </mesh>
    </shape>
    <shape ref_material="Ground">
      <mesh>
        <obj file="./Dragon_assets/Sand.obj" />
      </mesh>
    </shape>
    <shape ref_material="Scales">
      <mesh>
        <obj file="./Dragon_assets/Scales.obj" />
      </mesh>
    </shape>
    <shape ref_material="Rock">
      <mesh>
        <obj file="./Dragon_assets/SmallRock.obj" />
      </mesh>
    </shape>
    <shape ref_material="Support">
      <mesh>
        <obj file="./Dragon_assets/Support.obj" />
      </mesh>
    </shape>
    <shape ref_material="Copper">
      <mesh>
        <obj file="./Dragon_assets/SwordDecoration.obj" />
      </mesh>
    </shape>
    <shape ref_material="Gold">
      <mesh>
        <obj file="./Dragon_assets/SwordDecoration2.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpMetal">
      <mesh>
        <obj file="./Dragon_assets/SwordDecoration3.obj" />
      </mesh>
    </shape>
    <shape ref_material="Gold">
      <mesh>
        <obj file="./Dragon_assets/SwordGuard.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughMetal">
      <mesh>
        <obj file="./Dragon_assets/SwordHandle.obj" />
      </mesh>
    </shape>
    <shape ref_material="Teeth">
      <mesh>
        <obj file="./Dragon_assets/Teeth.obj" />
      </mesh>
    </shape>
  </shapes>
  <ext_resources>
    <resources_path path="../../envmaps/"/>

    <envmaps>
      <panoramic power_scaling="1.0" file="green_point_park_4k.hdr" name="my_envmap"></panoramic>
      <panoramic power_scaling="1.0" file="green_point_park_SPEC_32.hdr" name="my_envmap_spectral"></panoramic>

    </envmaps>
  </ext_resources>

  <background type="envmap" ref_envmap="my_envmap" ref_envmap_spectral="my_envmap_spectral"></background>

</scene>
