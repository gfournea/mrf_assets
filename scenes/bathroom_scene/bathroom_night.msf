<scene>
  <lights>
    <sphere_light radius="0.05">
      <emittance name="Point" type="uniform">
        <radiance value="60.0" />
        <spectrum file="../../light_sources/d65div100.spd" />
      </emittance>
      <position x="0.0" y="3.12" z="-0.0" />
    </sphere_light>
  </lights>

  <materials>

  <!--GGX materials made from reftractive index info from https://refractiveindex.info using B=450nm, G = 550nm, R = 650nm -->
  <!-- Correspondance: eta = n, kappa = k. n+ik = eta + i*kappa -->

    <material name="Wall" type="Lambert">
      <!-- <rgb_color r="1.0" g="1.0" b="1.0" /> -->
      <spectrum file="../../macbeth_spectrum/19_white.spd" />
      <albedo value="1.0" />
    </material>

    <material name="BlackAbsorber" type="Lambert">
      <rgb_color r="0.0" g="0.0" b="0.0" />
      <spectrum value="0:0.0, 1000:0.0" />
      <albedo value="1.0" />
    </material>

    <material alpha_g="0.2" name="RoughAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.001" name="SmoothAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.5" name="RoughGold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material name="SmoothCeramic" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/21_neutral65.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.5"/>
        <!-- <rgb_color r="1.0" g="1.0" b="1.0" /> -->
        <spectrum file="../../macbeth_spectrum/19_white.spd" />
      </specular>
    </material>

    <material name="DarkBorder" type="PhongNormalized">
      <diffuse>
        <albedo value="0.5"/>
        <!-- <rgb_color r="0.33" g="0.33" b="0.33" /> -->
        <spectrum file="../../macbeth_spectrum/23_neutral35.spd" />
      </diffuse>
      <specular exponent="15">
        <albedo value="0.5"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/21_neutral65.spd" />
      </specular>
    </material>

    <material name="DarkPlastic" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <!-- <rgb_color r="0.33" g="0.33" b="0.33" /> -->
        <spectrum file="../../macbeth_spectrum/23_neutral35.spd" />
      </diffuse>
      <specular exponent="25">
        <albedo value="0.25"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/21_neutral65.spd" />
      </specular>
    </material>

    <material name="Label" type="PhongNormalizedTextured">
      <diffuse>
        <albedo value="0.85"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum value="0:32.0,1000:32.0" />
      </diffuse>
      <diffuse_texture file="./textures/Label_BaseColor.png"/>
      <diffuse_texture_spectral file="./textures/Label_BaseColor_SPEC32.hdr"/>
      <specular exponent="25">
        <albedo value="0.15"/>
        <!-- <rgb_color r="1.0" g="1.0" b="1.0" /> -->
        <spectrum file="../../macbeth_spectrum/19_white.spd" />
      </specular>
    </material>

    <material name="Wood" type="PhongNormalizedTextured">
      <diffuse>
        <albedo value="0.95"/>
        <!-- <rgb_color r="0.47" g="0.33" b="0.27" /> -->
        <!-- <spectrum file="../../macbeth_spectrum/brown.spd" /> -->
        <spectrum value="0:32.0,1000:32.0" />
      </diffuse>
      <diffuse_texture file="./textures/kitchen_wood_diff_1k.png"/>
      <diffuse_texture_spectral file="./textures/kitchen_wood_diff_1k_SPEC32.hdr"/>
      <normal_map file="./textures/kitchen_wood_nor_1k.png"/>
      <exponent_map file="./textures/kitchen_wood_rough_1k.png"/>
      <specular exponent="250">
        <albedo value="0.05"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/19_white.spd" />
        <!-- <spectrum value="0:32.0,1000:32.0" /> -->
      </specular>
    </material>

    <material name="Towel" type="PhongNormalized">
      <diffuse>
        <albedo value="0.95"/>
        <!-- <rgb_color r="0.29" g="0.37" b="0.68" /> -->
        <spectrum file="../../macbeth_spectrum/8_purplishblue.spd" />
      </diffuse>
      <specular exponent="5">
        <albedo value="0.05"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/21_neutral65.spd" />
      </specular>
    </material>

    <material name="MarbleFloor" type="PhongNormalizedTextured">
      <diffuse>
        <albedo value="0.85"/>
        <!-- <rgb_color r="1.0" g="1.0" b="1.0" /> -->
        <!-- <spectrum file="../../macbeth_spectrum/19_white.spd" /> -->
        <spectrum value="0:32.0,1000:32.0" />
      </diffuse>
      <diffuse_texture file="./textures/marble_01_diff_1k.png"/>
      <diffuse_texture_spectral file="./textures/marble_01_diff_1k_SPEC32.hdr"/>
      <normal_map file="./textures/marble_01_nor_1k.png"/>
      <specular_texture file="./textures/marble_01_spec_1k.png"/>
      <specular_texture_spectral file="./textures/marble_01_spec_1k_SPEC32.hdr"/>
      <exponent_map file="./textures/marble_01_rough_1k.png"/>
      <specular exponent="250">
        <albedo value="0.15"/>
        <!-- <rgb_color r="0.65" g="0.65" b="0.65" /> -->
        <spectrum file="../../macbeth_spectrum/19_white.spd" />
        <!-- <spectrum value="0:32.0,1000:32.0" /> -->
      </specular>
    </material>
  </materials>
  <shapes>
    <shape ref_material="RoughAlu">
      <mesh>
        <obj file="./bathroom_assets/Bin.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughGold">
      <mesh>
        <obj file="./bathroom_assets/BlackWoodLacquer.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wall">
      <mesh>
        <obj file="./bathroom_assets/Bottle.obj" />
      </mesh>
    </shape>
    <shape ref_material="SmoothCeramic">
      <mesh>
        <obj file="./bathroom_assets/Ceramic.obj" />
      </mesh>
    </shape>
    <shape ref_material="DarkBorder">
      <mesh>
        <obj file="./bathroom_assets/DarkBorder.obj" />
      </mesh>
    </shape>
    <shape ref_material="DarkPlastic">
      <mesh>
        <obj file="./bathroom_assets/DarkPlastic.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughAlu">
      <mesh>
        <obj file="./bathroom_assets/Hinge.obj" />
      </mesh>
    </shape>
    <shape ref_material="Label">
      <mesh>
        <obj file="./bathroom_assets/Label.obj" />
      </mesh>
    </shape>
    <shape ref_material="SmoothAlu">
      <mesh>
        <obj file="./bathroom_assets/Mirror.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wood">
      <mesh>
        <obj file="./bathroom_assets/MirrorSupport.obj" />
      </mesh>
    </shape>
    <shape ref_material="DarkPlastic">
      <mesh>
        <obj file="./bathroom_assets/Plastic.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wood">
      <mesh>
        <obj file="./bathroom_assets/Shelf.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughAlu">
      <mesh>
        <obj file="./bathroom_assets/StainlessRough.obj" />
      </mesh>
    </shape>
    <shape ref_material="Towel">
      <mesh>
        <obj file="./bathroom_assets/Towel.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wall">
      <mesh>
        <obj file="./bathroom_assets/Wall.obj" />
      </mesh>
    </shape>
    <shape ref_material="BlackAbsorber">
      <mesh>
        <obj file="./bathroom_assets/WallOuter.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wood">
      <mesh>
        <obj file="./bathroom_assets/Wood.obj" />
      </mesh>
    </shape>    
    <shape ref_material="Wood">
      <mesh>
        <obj file="./bathroom_assets/Window.obj" />
      </mesh>
    </shape>
    <shape ref_material="MarbleFloor">
      <mesh>
        <obj file="./bathroom_assets/WoodFloor.obj" />
      </mesh>
    </shape>
    <shape ref_material="Towel">
      <mesh>
        <obj file="./bathroom_assets/LampStuff.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wall">
      <mesh>
        <obj file="./bathroom_assets/LampStuffInt.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughAlu">
      <mesh>
        <obj file="./bathroom_assets/Cylinder.obj" />
      </mesh>
    </shape>
  </shapes>

  <!-- <ext_resources> -->
    <!-- <resources_path path="../../../mrf_envmap_assets/HDRHaven/Night/"/> -->

    <!-- <envmaps> -->

      <!-- <panoramic power_scaling="10.0" file="preller_drive_2k.hdr"      name="my_envmap" /> -->
      <!-- <panoramic power_scaling="10.0" file="preller_drive_2k_SPEC16.hdr" name="my_envmap_spectral" /> -->

    <!-- </envmaps> -->
  <!-- </ext_resources> -->
  
  <ext_resources>
    <resources_path path="./textures/"/>

    <envmaps>
      <panoramic power_scaling="1.0" file="sunset_3.exr" name="my_envmap" />
      <panoramic power_scaling="10.0"file="sunset_3.exr" name="my_envmap_spectral" />

    </envmaps>
  </ext_resources>

  <background type="envmap" ref_envmap="my_envmap" ref_envmap_spectral="my_envmap_spectral"></background>

  <background type="envmap" ref_envmap="my_envmap" ref_envmap_spectral="my_envmap_spectral"></background>
  <!-- <background type="uniform"> -->
    <!-- <spectrum value="0:0.05087608844041824,1000:0.05087608844041824" /> -->
    <!-- <rgb_color b="0.05087608844041824" g="0.05087608844041824" r="0.05087608844041824" /> -->
  <!-- </background> -->

</scene>
