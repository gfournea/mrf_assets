<scene>
  <!-- <lights> -->
    <!-- <sphere_light radius="0.10000000149011612"> -->
      <!-- <emittance name="Lamp" type="uniform"> -->
        <!-- <radiance value="100.0" /> -->
        <!-- <rgb_color b="1.0" g="1.0" r="1.0" /> -->
        <!-- <spectrum value="0:1.0,1000:1.0" /> -->
      <!-- </emittance> -->
      <!-- <position x="4.076245307922363" y="5.903861999511719" z="-1.0054539442062378" /> -->
    <!-- </sphere_light> -->
  <!-- </lights> -->
  
  <materials>

    <material name="default_lambert" type="Lambert">
        <albedo value="1.0"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
    </material>

    <!----------------------------------------------------------------------------------->
    <!-- METAL PART -->
    <!----------------------------------------------------------------------------------->
    

    <material alpha_g="0.05" name="SharpIron" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material alpha_g="0.32" name="RoughIron" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Fe/Johnson_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material alpha_g="0.15" name="Copper" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Cu/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Cu/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.01" name="SharpAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material alpha_g="0.2" name="RoughAlu" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Al/McPeak_kappa.spd" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.05" name="SharpGold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    <material alpha_g="0.15" name="RoughGold" type="ggx">
      <fresnel>
        <eta>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="../../mrf_assets/refractive_index/data/main/Au/Babar_kappa.spd" />
        </kappa>
      </fresnel>
    </material>
    
    <material name="Wood" type="PhongNormalizedTextured" texture_repeat="2.0">
      <diffuse>
        <albedo value="0.75"/>        
      <!-- Problem with RGB2SPEC, need to scale the texture by its band number and force rgb = 1-->
        <spectrum value="0:2.0,1000:2.0" />
        <rgb_color b="1.0" g="1.0" r="1.0" />
      </diffuse>
      <diffuse_texture file="../../mrf_envmap_assets/textures/kitchen_wood_diff_1k.png"/>
      <diffuse_texture_spectral file="../../mrf_envmap_assets/textures/kitchen_wood_diff_1k_SPEC32.hdr"/>
      <normal_map file=".../../mrf_envmap_assets/textures/kitchen_wood_nor_1k.png"/>
      <exponent_map file="../../mrf_envmap_assets/textures/kitchen_wood_rough_1k.png"/>
      <specular exponent="250">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>

    <!----------------------------------------------------------------------------------->
    <!-- GLASS MAT -->
    <!----------------------------------------------------------------------------------->
    

    <material alpha_g="0.2" name="RedGlass" type="walter_bsdf">
      <extinction>
        <spectrum file="./tea_time_assets/spd/red.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>

    <material alpha_g="0.2" name="GreenGlass" type="walter_bsdf">
      <extinction>
        <spectrum file="./tea_time_assets/spd/green.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>    
    
    <material alpha_g="0.2" name="BlueGlass" type="walter_bsdf">
      <extinction>
        <spectrum file="./tea_time_assets/spd/blue.spd" />
      </extinction>
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>        
    
    <!-- <material alpha_g="0.1" name="CustomGlass" type="walter_bsdf"> -->
    <material name="CustomGlass" type="FresnelGlass">
      <fresnel>
        <eta>
          <spectrum file="./tea_time_assets/spd/dielec_custom_eta.spd" />
        </eta>
        <kappa>
          <spectrum file="./tea_time_assets/spd/dielec_custom_kappa.spd" />
        </kappa>
      </fresnel>
    </material>        
    
    <material alpha_g="0.1" name="WhiteGlass" type="walter_bsdf">
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>    
    
    <material name="PerfectGlass" type="FresnelGlass">
      <fresnel>
        <eta>
          <spectrum value="0:1.45,1000:1.45" />
        </eta>
        <kappa>
          <spectrum value="0:0,1000:0" />
        </kappa>
      </fresnel>
    </material>
    
    <!----------------------------------------------------------------------------------->
    <!-- MACBETH MAT -->
    <!----------------------------------------------------------------------------------->
    
    <material name="1_darkskin" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/1_darkskin.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="2_lightskin" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/2_lightskin.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="3_bluesky" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/3_bluesky.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="4_foliage" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/4_foliage.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="5_blueflower" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/5_blueflower.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="6_bluishgreen" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/6_bluishgreen.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="7_orange" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/7_orange.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="8_purplishblue" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/8_purplishblue.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="9_lightred" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/9_lightred.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="10_purple" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/10_purple.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="11_yellowgreen" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/11_yellowgreen.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="12_orangeyellow" type="PhongNormalized">
            <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/12_orangeyellow.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="13_blue" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/13_blue.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="14_green" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/14_green.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="15_red" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/15_red.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="16_yellow" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/16_yellow.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="17_magenta" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/17_magenta.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="18_cyan" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/18_cyan.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="19_white" type="PhongNormalized">
            <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>

    <material name="19_white_d" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
    </material>
    
    <material name="20_neutral8" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/20_neutral8.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="21_neutral65" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/21_neutral65.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="22_neutral5" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/22_neutral5.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="23_neutral35" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/23_neutral35.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>
    
    <material name="24_black" type="PhongNormalized">
      <diffuse>
        <albedo value="0.75"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/24_black.spd" />
      </diffuse>
      <specular exponent="50">
        <albedo value="0.25"/>
        <spectrum file="../../mrf_assets/macbeth_spectrum/19_white.spd" />
      </specular>
    </material>    
    
    <material name="24_black_d" type="Lambert">
      <albedo value="1.0"/>
      <spectrum file="../../mrf_assets/macbeth_spectrum/24_black.spd" />
    </material>

  </materials>
  <shapes>
    <shape ref_material="SharpAlu">
      <mesh>
        <obj file="./tea_time_assets/base.obj" />
      </mesh>
    </shape>
    <shape ref_material="18_cyan">
      <mesh>
        <obj file="./tea_time_assets/bow_plate.obj" />
      </mesh>
    </shape>
    <shape ref_material="15_red">
      <mesh>
        <obj file="./tea_time_assets/CoffeeCup.obj" />
      </mesh>
    </shape>
    <shape ref_material="8_purplishblue">
      <mesh>
        <obj file="./tea_time_assets/CoffeeCup1.obj" />
      </mesh>
    </shape>
    <shape ref_material="10_purple">
      <mesh>
        <obj file="./tea_time_assets/CoffeeCup2.obj" />
      </mesh>
    </shape>
    <shape ref_material="12_orangeyellow">
      <mesh>
        <obj file="./tea_time_assets/CoffeeCup3.obj" />
      </mesh>
    </shape>
    <!-- <shape ref_material="WhiteGlass"> -->
    <shape ref_material="RedGlass">
      <mesh>
        <obj file="./tea_time_assets/globe.obj" />
      </mesh>
    </shape>
    <shape ref_material="Copper">
      <mesh>
        <obj file="./tea_time_assets/happy.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpGold">
      <mesh>
        <obj file="./tea_time_assets/lucy_150k_faces_scaledUC.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpGold">
      <mesh>
        <obj file="./tea_time_assets/plate_tea_cylinder.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpAlu">
      <mesh>
        <obj file="./tea_time_assets/plate_tea_service.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpIron">
      <mesh>
        <obj file="./tea_time_assets/square_plate.obj" />
      </mesh>
    </shape>
    <shape ref_material="Copper">
      <mesh>
        <obj file="./tea_time_assets/support.obj" />
      </mesh>
    </shape>
    <shape ref_material="19_white">
      <mesh>
        <obj file="./tea_time_assets/support_sphere.obj" />
      </mesh>
    </shape>
    <shape ref_material="Wood">
      <mesh>
        <obj file="./tea_time_assets/table.obj" />
      </mesh>
    </shape>
    <shape ref_material="19_white_d">
      <mesh>
        <obj file="./tea_time_assets/tablehold.obj" />
      </mesh>
    </shape>
    <shape ref_material="24_black">
      <mesh>
        <obj file="./tea_time_assets/teapot_bird.obj" />
      </mesh>
    </shape>
    <shape ref_material="RoughIron">
      <mesh>
        <obj file="./tea_time_assets/teapot_body.obj" />
      </mesh>
    </shape>
    <!-- <shape ref_material="24_black"> -->
    <shape ref_material="24_black_d">
      <mesh>
        <obj file="./tea_time_assets/teapot_handle.obj" />
      </mesh>
    </shape>
    <!-- <shape ref_material="24_black"> -->
    <shape ref_material="24_black">
      <mesh>
        <obj file="./tea_time_assets/teapot_top.obj" />
      </mesh>
    </shape>
    <shape ref_material="SharpAlu">
      <mesh>
        <obj file="./tea_time_assets/teapot_wire.obj" />
      </mesh>
    </shape>
    <shape ref_material="GreenGlass">
      <mesh>
        <obj file="./tea_time_assets/vase_design.obj" />
      </mesh>
    </shape>
    <shape ref_material="1_darkskin">
      <mesh>
        <obj file="./tea_time_assets/vase_pipe1.obj" />
      </mesh>
    </shape>
    <shape ref_material="BlueGlass">
      <mesh>
        <obj file="./tea_time_assets/vase_pipe2.obj" />
      </mesh>
    </shape>
    <shape ref_material="2_lightskin">
      <mesh>
        <obj file="./tea_time_assets/vase_pipe4.obj" />
      </mesh>
    </shape>
    <shape ref_material="CustomGlass">
      <mesh>
        <obj file="./tea_time_assets/xyz-dragon-2M-loop.obj" />
      </mesh>
    </shape>
    <!-- <shape ref_material="Copper"> -->
    <!-- <shape ref_material="default_lambert"> -->
    <shape ref_material="PerfectGlass">
      <mesh>
        <obj file="./tea_time_assets/xyzrgb_statuette.obj" />
      </mesh>
    </shape>
  </shapes>
  
      <ext_resources>
    <resources_path path="../../mrf_envmap_assets/HDRHaven/Outdoor"/>

    <envmaps>

      <panoramic power_scaling="10.0" file="chinese_garden_2k.hdr"      name="my_envmap" phi="180" theta="0"/>
      <panoramic power_scaling="10.0" file="chinese_garden_2k_SPEC16.hdr" name="my_envmap_spectral" phi="180" theta="0"/>

    </envmaps>
  </ext_resources>  
  
  <background type="envmap" ref_envmap="my_envmap" ref_envmap_spectral="my_envmap_spectral"></background>
</scene>
