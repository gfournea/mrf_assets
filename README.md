Introduction
============

This repository contains example scenes for MRF (Malia Rendering Framework).
For more information about MRF, please visit [the official MRF website](https://pacanows.gitlabpages.inria.fr/MRF/main.md.html).
You can also see the [MRF GitLab repository](https://gitlab.inria.fr/pacanows/MRF).


How to use
==========

Each scene is made of:
- a `.msf` file describing the scene configuration
- a `.mcf` file for camera specifications
- a set of additional files for assets refered from the `.msf` file.

A script `render_all.py` is provided to render all the scenes (`.msf` files) contained in the `scenes` directory. You need to specify in the script file the path of your malia and mic installation (part of the MRF toolkit) if not in your PATH.

Additional Spectral assets
==========

Additional spectral envmaps and textures can be downloaded from our [Spectral assets repository](https://gitlab.inria.fr/dmurray/mrf_envmap_assets).
